<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 21.11.17
 * Time: 20:56
 */
namespace AppBundle\Libs;

use Doctrine\ORM\EntityManager;
use phpQuery;

class CourseManager
{

    private $entityManager;

    public function __construct(EntityManager $entityManager = null)
    {
        $this->entityManager = $entityManager;
    }

    public function saveCourseRates()
    {
        $data = $this->getDataFromSitePage();
        foreach ($data as $key=>$value) {
            $this->entityManager
                ->getRepository('AppBundle:Currency')
                ->saveNewCurrency(
                    $key,
                    $data[$key][0],
                    $data[$key][1],
                    $data[$key][2]
                );
        }
    }

    public function UpdateCourseSchedule()
    {
        $fresh_rates = $this->getDataFromSitePage();
        $old_table = $this->getCoursesDataFromDB();

        foreach ($old_table as $item){
            foreach ($fresh_rates as $rate) {
                if($item->getTitleAlias() == $rate[0]){

                    if($item->getBuyRate() != $rate[1]){
                        $item->setBuyRate($rate[1]);
                    }
                    if($item->getSellRate() != $rate[2]){
                        $item->setSellRate($rate[2]);
                    }
                    $item->setStartDate(new \DateTime());

                    $this->entityManager->flush();
                }
            }
        }
    }

    public function getCoursesDataFromDB()
    {
        return  $this->entityManager->getRepository('AppBundle:Currency')->findAll();
    }

    /**
     * @return array
     */
    public function getDataFromSitePage()
    {
        $html = $this->getCurl('http://www.bakai.kg/');

        $doc = phpQuery::newDocument($html);

        $f_table = $doc->find('.no-border.curr-table:eq(0)');
        $rates = [];
        $table = pq($f_table);
        $table->find('tr:first')->remove();
        $tr_0 = pq($table->find('tr:eq(0)'));
        $tr_1 = pq($table->find('tr:eq(1)'));
        $tr_2 = pq($table->find('tr:eq(2)'));
        $tr_3 = pq($table->find('tr:eq(3)'));
        $rates ['Американский доллар'] = [
            $tr_0->find('.first')->text(),
            $tr_0->find('td:eq(0)')->text(),
            $tr_0->find('td:eq(1)')->text()
        ];
        $rates ['Евро'] = [
            $tr_1->find('.first')->text(),
            $tr_1->find('td:eq(0)')->text(),
            $tr_1->find('td:eq(1)')->text()
        ];
        $rates ['Российский рубль'] = [
            $tr_2->find('.first')->text(),
            $tr_2->find('td:eq(0)')->text(),
            $tr_2->find('td:eq(1)')->text()
        ];
        $rates ['Казахский тенге'] = [
            $tr_3->find('.first')->text(),
            $tr_3->find('td:eq(0)')->text(),
            $tr_3->find('td:eq(1)')->text()
        ];
        return $rates;
    }


    public function getCurl($url, $referer = 'https://www.google.com/')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 
        (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0");
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

}