<?php

namespace AppBundle\Controller;

use phpQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Libs\CourseManager;

class CurrencyController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function showCourseAction()
    {
        $courses = $this->getDoctrine()
            ->getRepository('AppBundle:Currency')
            ->findAll();

        $course_manager = $this->getCourseManager();

        if (empty($courses)) {
            $course_manager->saveCourseRates();
            $courses = $course_manager->getCoursesDataFromDB();
        }

        return $this->render('@App/index.html.twig', array(
            'courses' => $courses,
        ));
    }

    /**
     * @Route("/update_schedule")
     * @Method("GET")
     */
    public function updateCourseSchedule()
    {
        $course_manager = $this->getCourseManager();
        $course_manager->UpdateCourseSchedule();

        return new Response(json_encode([
            'success' => $success ?? true
        ]));
    }


    public function getCourseManager()
    {
        return new CourseManager($this->getDoctrine()->getManager());
    }

}
