<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CurrencyRepository")
 */
class Currency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="title_alias", type="string")
     */
    private $title_alias;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_rate", type="float")
     */
    private $sell_rate;

    /**
     * @var float
     * @ORM\Column(name="buy_rate", type="float")
     */
    private $buy_rate;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $start_date;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Currency
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleAlias
     *
     * @param string $titleAlias
     *
     * @return Currency
     */
    public function setTitleAlias($titleAlias)
    {
        $this->title_alias = $titleAlias;

        return $this;
    }

    /**
     * Get titleAlias
     *
     * @return string
     */
    public function getTitleAlias()
    {
        return $this->title_alias;
    }

    /**
     * @return float
     */
    public function getSellRate(): float
    {
        return $this->sell_rate;
    }

    /**
     * @param float $sell_rate
     */
    public function setSellRate(float $sell_rate)
    {
        $this->sell_rate = $sell_rate;
    }

    /**
     * @return float
     */
    public function getBuyRate(): float
    {
        return $this->buy_rate;
    }

    /**
     * @param float $buy_rate
     */
    public function setBuyRate(float $buy_rate)
    {
        $this->buy_rate = $buy_rate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param \DateTime $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

}

