
$(document).ready(function () {
    var update_btn = $("#update_btn");
    var load_btn = $("#load_btn");

    load_btn.click(function () {
        $("#course_table").css({
            'display': 'block'
        });
        $("#question").hide();
        load_btn.hide();
        update_btn.show();
    });

    update_btn.click(function () {
        get('/update_schedule', function (response) {
            if (!response.success) {
                throw 'Server Error';
            } else {
                $("#fresh_greeting").html('Теперь он точно свежий');
            }
        });
    });
    function get (route, action) {
        $.get(route, action, 'json')
    }
});